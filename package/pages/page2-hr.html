
<p>Prvi skup parametara čine granice istosmjerne i izmjenične smetnje. Obje granice razmatramo na jednostavnom primjeru kod kojeg izlaz jednog digitalnog sklopa pobuđuje (odnosno spojen je na) ulaz drugog digitalnog sklopa.</p>

<h3>Granica istosmjerne smetnje</h3>

<p>Najprije ćemo se upoznati s granicom istosmjerne smetnje. Slika 1 prikazuje izlaz jednog digitalnog sklopa koji je spojen na ulaz drugog digitalnog sklopa.</p>

<figure class="mlFigure">
<img src="@#file#(gs1.svg)" alt="Granica istosmjerne smetnje."><br>
<figcaption><span class="mlFigKey">Slika 1.</span> Granica istosmjerne smetnje.</figcaption>
</figure>

<p>Ispod lijevog sklopa čiji izlaz generira naponsku razinu koja se prenosi na ulaz desnog sklopa, prikazane su definicije naponskih razina na izlazu. Ispod desnog sklopa prikazane su definicije naponskih razina na ulazu.</p>

<p>Da bismo razumjeli zašto dolazi do ovakve definicije, razmotrimo na trenutak izlaz jednog konkretnog digitalnog sklopa; primjerice, uzmimo običan bipolarni invertor.</p>

<figure class="mlFigure">
<img src="@#file#(inv-bip.svg)" alt="Invertor pobuđuje drugi sklop."><br>
<figcaption><span class="mlFigKey">Slika 2.</span> Invertor pobuđuje drugi sklop. Unutrašnjost drugog sklopa trenutno nije važna.</figcaption>
</figure>

<p>Kada izlaz invertora treba biti logičko 1 (vidi sliku 3), tranzistor je isključen, pa struja ne teče od izvora kroz R<sub>C</sub> pa kroz tranzistor prema masi.</p>

<figure class="mlFigure">
<img src="@#file#(inv-bip1.svg)" alt="Invertor pobuđuje drugi sklop: izlaz je u 1."><br>
<figcaption><span class="mlFigKey">Slika 3.</span> Invertor pobuđuje drugi sklop: izlaz je u 1.</figcaption>
</figure>

<p>Međutim, ulaz desnog sklopa sada se ponaša kao trošilo, pa struja teče iz izvora napajanja kroz otpornik R<sub>C</sub>, kroz vodič kojim je izlaz lijevog sklopa spojen s ulazom desnog sklopa i potom u desnom sklopu kroz ekvivalentni otpor ulaza prema masi (prikazano crvenom crtkanom linijom). Izlazni napon \(U_f\) pri tome je određen izrazom:
$$
U_f = U - U_{R_C} = U - I \cdot R_C.
$$
Kada je \(I=0\), izlazni napon je maksimalan i jednak je naponu napajanja. S porastom struje \(I\), u skladu s prethodnim izrazom, napon na izlazu lijevog sklopa sve će više padati. Ovaj hod napona na izlazu prikazan je na slici 1 (lijevo) i predstavlja područje logičke jedinice na izlazu. Proizvođač modula za izlaz digitalnog sklopa definira dva parametra.
</p>
<ul>
<li>\(U_{\text{OHmin}}\) je minimalni napon na koji može pasti napon na izlazu sklopa kada izlaz generira logičku jedinicu i dok sklop radi u dozvoljenom režimu rada.</li>
<li>\(I_{\text{OHmax}}\) je maksimalna struja koju trošilo smije potegnuti iz izlaza sklopa a uz koju se garantira da napon na izlazu sklopa neće pasti ispod napona \(U_{\text{OHmin}}\).</li>
</ul>

<p>Uočite kako su ova dva parametra u prikazanom slučaju međusobno povezana. Također, uočite da se u ovom scenariju lijevi sklop ponaša kao izvor koji napaja desni sklop (koji je za njega trošilo).</p>

<p>Za desni sklop u opisanom scenariju (kada je ulaz sklopa u logičkoj jedinici), proizvođač također definira dva parametra.</p>
<ul>
<li>\(U_{\text{IHmin}}\) je minimalni napon koji se smije pojaviti na ulazu digitalnog sklopa a koji će u sklopu biti ispravno protumačen kao logička vrijednost 1.</li>
<li>\(I_{\text{IHmax}}\) je maksimalna struja koju će ulaz digitalnog sklopa potegnuti ako je na ulazu logička jedinica.</li>
</ul>

<p>Primjetite da sigurno mora vrijediti da je \(U_{\text{IHmin}} \leq U_{\text{OHmin}}\). Naime, u redovnom radu, za logičku vrijednost 1 izlaz sklopa će generirati napone koji od napona napajanja mogu pasti sve do napona \(U_{\text{OHmin}}\). Tada ulaz digitalnog sklopa taj čitav raspon napona mora biti u stanju ispravno protumačiti. No od ulaza bismo htjeli i bolje od toga: ako nastupi najgora situacija pa izlaz generira upravo napon iznosa \(U_{\text{OHmin}}\), htjeli bismo ostvariti i robusnost na pojavu smetnje koja će napon na izlazu dodatno spustiti. Iz tog razloga, želimo da je vrijedi relacija "strogo-manje", odnosno \(U_{\text{IHmin}} &lt; U_{\text{OHmin}}\) (dakle, da je ulaz sklopa tolerantniji). Koliko najviše smetnja smije spustiti napon na izlazu a da ga se i dalje na ulazu ispravno protumači kao logičko 1 određeno je upravo razlikom ta dva napona i naziva se <em>granica smetnje na visokoj razini</em>:
$$
 U_{\text{GSH}} = U_{\text{OHmin}} - U_{\text{IHmin}}.
$$
Pronađite gdje je ova granica označena na slici 1.
</p>

<p>Pogledajmo sada drugi slučaj, u kojem je na izlazu invertora logička nula - slika 4.</p>

<figure class="mlFigure">
<img src="@#file#(inv-bip0.svg)" alt="Invertor pobuđuje drugi sklop: izlaz je u 0."><br>
<figcaption><span class="mlFigKey">Slika 4.</span> Invertor prima struju drugog sklopa: izlaz je u 0.</figcaption>
</figure>

<p>U ovoj situaciji tranzistor je uključen i na izlazu održava nisku naponsku razinu. Baznim otpornikom podešena je odgovarajuća struja koja definira maksimalni iznos kolektorske struje koju tranzistor može progutati. U prikazanoj situaciji, vodić kojim je izlaz lijevog sklopa povezan s ulazom desnog sklopa je na relativno niskom potencijalu pa se sada okreće tok struje u desnom sklopu: struja uvijek teče od točke višeg potencijala prema točki nižeg potencijala što znači da će sada struja izlaziti iz ulaza desnog sklopa i ulaziti u izlaz lijevog sklopa (znam, rečenica je grozna, ali pogledajte ponovno sliku 4). Desni se sklop sada ponaša kao izvor koji napaja lijevi sklop.</p>

<p>U lijevom sklopu sada vrijedi:
$$
 I_C = I_{RC} + I
$$
gdje struju <i>I</i> generira desni sklop. Kolektorska struja ima definiran maksimum koji je određen podešenom baznom strujom. Stoga će u određenom trenutku, kako struja <i>I</i> bude rasla početi rasti i napon između kolektora i emitera (a to je ujedno i izlazni napon). Ovaj hod napona na izlazu od vrlo malog pa prema sve višim prikazan je na slici 1 lijevo i označen kao područje logičke nule. Proizvođač modula definira dva parametra. 
</p>

<ul>
<li>\(U_{\text{OLmax}}\) je maksimalni napon na koji može porasti napon na izlazu sklopa kada izlaz generira logičku nulu i dok sklop radi u dozvoljenom režimu rada.</li>
<li>\(I_{\text{OLmax}}\) je maksimalna struja koju desni sklop smije tjerati u izlaza lijevog sklopa a uz koju se garantira da napon na izlazu lijevog sklopa neće porasti iznad napona \(U_{\text{OLmax}}\).</li>
</ul>

<p>Za desni sklop u opisanom scenariju (kada je ulaz sklopa u logičkoj nuli), proizvođač također definira dva parametra.</p>
<ul>
<li>\(U_{\text{ILmax}}\) je maksimalni napon koji se smije pojaviti na ulazu digitalnog sklopa a koji će u sklopu i dalje biti ispravno protumačen kao logička vrijednost 0.</li>
<li>\(I_{\text{ILmax}}\) je maksimalna struja koju će ulaz digitalnog sklopa generirati ako je na ulazu logička nula.</li>
</ul>

<p>Kako se u normalnom režimu rada, kada je izlaz sklopa u logičkoj nuli na njemu mogu pojaviti naponi od minimalnih pa sve do \(U_{\text{OLmax}}\), očekujemo da sve te napone desni sklop ispravno protumači kao logičku nulu. Stoga mora vrijediti da je \(U_{\text{ILmax}} \geq U_{\text{ILmax}}\). Pri tome bismo htjeli imati i određenu marginu sigurnosti odnosno otpornosti na djelovanje smetnje koja u situaciji kada je izlazni napon već na svojem maksimumu, isti mogu još malo podignuti. Stoga mora biti \(U_{\text{ILmax}} &gt; U_{\text{ILmax}}\), pri čemu je <em>granica smetnje na niskoj razini</em> definirana upravo kao razlika ta dva napona:
$$
 U_{\text{GSL}} = U_{\text{ILmax}} - U_{\text{OLmax}}.
$$
Pronađite gdje je ova granica označena na slici 1.
</p>

<p>Ako su granice istosmjerne smetnje na oba kraja jednake, onda je i <em>granica istosmjerne smetnje</em> (jedan kumulativni parametar) upravo tolika. U situaciji kada je \(U_{\text{GSL}}\) različit od \(U_{\text{GSH}}\), tada se kao ukupna granica istosmjerne smetnje uzima njihov minimum:
$$
 U_{\text{GS}} = \min (U_{\text{GSL}}, U_{\text{GSH}}).
$$
Opravdanje za minimum bi trebalo biti očito: sklop mora raditi ispravno i kada mu je izlaz u logičkoj nuli i kada mu je izlaz u logičkoj jedinici.</p>

<h3>Granica izmjenične smetnje</h3>

<p>Granica istosmjerne smetnje podrazumjeva trajno djelovanje smetnje. Granica izmjenične smetnje razmatra djelovanje smetnje određene amplitude i određenog trajanja, nakon čega smetnja nestaje. Ovdje nećemo raditi formalnu analizu djelovanja izmjeničnih smetnji već ćemo dati pojašnjenje na intuitivnoj razini.</p>

<p>U elektroničkim sklopovima nemoguće je postići trenutnu promjenu napona na izlazu sklopa. Uvijek postoje parazitne kapacitivnosti i otpori vodiča i elektroničkih elemenata koji će tvoriti RC-mreže. Iz tog razloga, ako se u nekom trenutku pojavi smetnja iznosa U<sub>S</sub>, napon na izlazu neće se trenutno uvećati/umanjiti za taj iznos (ovisno o predznaku smetnje), već će se postupno približavati tom novom iznosu. Što smetnja dulje traje, to će se napon na izlazu više približiti generiranom naponu na koji je superponiran čitav iznos smetnje. Pustimo li smetnju da traje do beskonačnosti, ona će sigurno promijeniti napon na punu sumu i tada će granica smetnje biti određena granicom istosmjerne smetnje. Međutim, ako djelovanje smetnje prestane prije toga, napon na izlazu nije se stigao promijeniti za puni iznos smetnje već samo za jedan njezin dio.</p>

<p>Kako smo analizom granice istosmjerne smetnje odredili koliko se maksimalno smije promijeniti napon na izlazu, slijedi da izmjenična smetnja smije imati i veću amplitudu od toga. Što smetnja traje kraće, to joj amplituda smije biti veća.</p>




